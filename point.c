#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "point.h"

float calcul(int Xa,int Ya, int Xb ,int Yb){
    float result = 0;
    result= sqrt(pow(Xb-Xa,2)+pow(Yb-Ya,2));
    return(result);
}

float peri(float result){
    struct Coord coord;
    coord.Ax= 2;
    coord.Ay= 0;
    coord.Bx= 6;
    coord.By= 0;
    coord.Cx= 10;
    coord.Cy= 2;
    coord.Dx= 8;
    coord.Dy= 6;
    coord.Ex= 2;
    coord.Ey= 8;
    coord.Fx= 0;
    coord.Fy= 4;

    coord.C1= calcul(coord.Ax,coord.Ay,coord.Bx,coord.By);
    coord.C2= calcul(coord.Bx,coord.By,coord.Cx,coord.Cy);
    coord.C3= calcul(coord.Cx,coord.Cy,coord.Dx,coord.Dy);
    coord.C4= calcul(coord.Dx,coord.Dy,coord.Ex,coord.Ey);
    coord.C5= calcul(coord.Ex,coord.Ey,coord.Fx,coord.Fy);
    coord.C6= calcul(coord.Fx,coord.Fy,coord.Ax,coord.Ay);
    result= coord.C1 + coord.C2 + coord.C3 + coord.C4 + coord.C5 + coord.C6;
    return(result);
}
