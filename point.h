#ifndef POINT_H_INCLUDED
#define POINT_H_INCLUDED

float calcul (int Xa,int Ya,int Xb,int Yb);
float peri(float result);

typedef struct Coord coord;

struct Coord{
    float Ax;
    float Ay;
    float Bx;
    float By;
    float Cx;
    float Cy;
    float Dx;
    float Dy;
    float Ex;
    float Ey;
    float Fx;
    float Fy;
    float C1,C2,C3,C4,C5,C6;
};

#endif // POINT_H_INCLUDED
